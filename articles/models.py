from django.db import models
from django.utils import timezone
import datetime


class Article(models.Model):
    art_title = models.CharField('Article name', max_length=200)
    art_text = models.TextField('Article text')
    pub_date = models.DateTimeField('Publication dara')

    def __str__(self):
        return self.art_title

    def was_pulished_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=7)

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField("author name", max_length=50)
    comment_text = models.CharField('Comment text', max_length=200)

    def __str__(self):
        return self.author_name
